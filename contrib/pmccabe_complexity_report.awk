#!/usr/bin/awk -f
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright 2021 (C) Olliver Schinagl <oliver@schinagl.nl>
# Copyright 2021 (C) Kevin Dekker <>
# Copyright 2021 (C) EVBox Intelligence B.V.

OUTSTANDING_SCORE = 3;
EXCELLENT_SCORE = 5;
VERY_GOOD_SCORE = 7;
GOOD_SCORE = 10;
MEDIOCRE_SCORE = 15;
POOR_SCORE = 20;
VERY_POOR_SCORE = 25;
APPALLING_SCORE = 50;

BEGIN {
        scores[OUTSTANDING_COMPLEXITY] = 0;
        scores[EXCELLENT_COMPLEXITY] = 0;
        scores[VERY_GOOD_COMPLEXITY] = 0;
        scores[GOOD_COMPLEXITY] = 0;
        scores[MEDIOCRE_COMPLEXITY] = 0;
        scores[POOR_COMPLEXITY] = 0;
        scores[VERY_POOR_COMPLEXITY] = 0;
        scores[APPALLING_COMPLEXITY] = 0;

	printf "+---------------------------------------------------------------+\n"
	printf "|------------- McCabe Cyclomatic Complexity report -------------|\n"
	printf "|---------------------------------------------------------------|\n"
	printf "|                            Scores                             |\n"
}

{
	NR <= 7 {
		next
	}

	$1 > 0 && $1 <= OUTSTANDING_SCORE {
		scores[OUTSTANDING_COMPLEXITY]++;
	}
	$1 > OUTSTANDING_SCORE && $1 <= EXCELLENT_SCORE {
		scores[EXCELLENT_COMPLEXITY]++;
	}
	$1 > EXCELLENT_SCORE && $1 <= VERY_GOOD_SCORE {
		scores[VERY_GOOD_COMPLEXITY]++;
	}
	$1 > VERY_GOOD_SCORE && $1 <= GOOD_SCORE {
		scores[GOOD_COMPLEXITY]++;
	}
	$1 > GOOD_SCORE && $1 <= MEDIOCRE_SCORE {
		scores[MEDIOCRE_COMPLEXITY]++;
	}
	$1 > MEDIOCRE_SCORE && $1 <= POOR_SCORE {
		scores[POOR_COMPLEXITY]++;
	}
	$1 > POOR_SCORE && $1 <= VERY_POOR_SCORE {
		scores[VERY_POOR_COMPLEXITY]++;
	}
	$1 > VERY_POOR_SCORE {
		scores[APPALLING_COMPLEXITY]++;
	}
}

END {
	printf "|   %-50s%7d   |\n", "Outstanding", scores[OUTSTANDING_COMPLEXITY];
	printf "|   %-50s%7d   |\n", "Excellent", scores[EXCELLENT_COMPLEXITY];
	printf "|   %-50s%7d   |\n", "Very Good", scores[VERY_GOOD_COMPLEXITY];
	printf "|   %-50s%7d   |\n", "Good", scores[GOOD_COMPLEXITY];
	printf "|   %-50s%7d   |\n", "Mediocre", scores[MEDIOCRE_COMPLEXITY];
	printf "|   %-50s%7d   |\n", "Poor", scores[POOR_COMPLEXITY];
	printf "|   %-50s%7d   |\n", "Very poor", scores[VERY_POOR_COMPLEXITY];
	printf "|   %-50s%7d   |\n", "Appalling", scores[APPALLING_COMPLEXITY];
	printf "+---------------------------------------------------------------+\n";
}
